<h1>Gulp Template</h1>


<h2>How to use Gulp Template</h2>

<ol>
	<li><a href="https://gitlab.com/rrustam91/gulp-template/-/archive/master/gulp-template-master.zip">Download</a><strong> Gulp template</strong> from Gitlab</li>
	<li>Install Node Modules: <strong>npm i</strong>;</li>
	<li>Run the template: <strong>gulp</strong>.</li>
</ol>

<h2>Gulp tasks:</h2>

<ul>
	<li><strong>gulp</strong>: run default gulp task (sass, js, watch, browserSync) for web development;</li>
	<li><strong>build</strong>: build project to <strong>dist</strong> folder (cleanup, image optimize, removing unnecessary files);</li>
	<li><strong>deploy</strong>: project deployment on the server from <strong>dist</strong> folder via <strong>FTP</strong>;</li>
	<li><strong>rsync</strong>: project deployment on the server from <strong>dist</strong> folder via <strong>RSYNC</strong>;</li>
	<li><strong>clearcache</strong>: clear all gulp cache.</li>
</ul>
 
 
 
<h1>Шаблон на Gulp</h1> 
<h2>Как использовать Gulp Template</h2>

<ol>
	<li><a href="https://gitlab.com/rrustam91/gulp-template/-/archive/master/gulp-template-master.zip">Скачайте</a><strong> Gulp шаблон</strong> с Gitlab</li>
	<li>Установите Node модули и компоненты через команду: <strong>npm i</strong>;</li>
	<li>Запустите шаблон через команду: <strong>gulp</strong>.</li>
</ol>

<h2>Gulp команды:</h2>

<ul>
	<li><strong>gulp</strong>: запускает дефолтный gulp (sass, js, watch, browserSync) для веб разработки;</li>
	<li><strong>build</strong>: компилирует проект в папку <strong>dist</strong> (cleanup, image optimize, removing unnecessary files);</li>
	<li><strong>deploy</strong>: деплоит проект на сервер из папки<strong>dist</strong> по <strong>FTP</strong>;</li>
	<li><strong>rsync</strong>: деплоит проект на сервер из папки <strong>dist</strong> через  <strong>RSYNC</strong>;</li>
	<li><strong>clearcache</strong>: Чистит весь кэш gulp(a).</li>
</ul>
 